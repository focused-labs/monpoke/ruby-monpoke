class GameController
  attr_accessor :game_state

  def initialize
    self.game_state = GameState.new
  end

  def exec_cmd (command)
    case command.type
    when :CREATE
      raise StandardError.new("HP and AP must be above 0") if command.ap && command.ap.to_i < 1 || command.hp && command.hp.to_i < 1
      monpoke = Monpoke.new(command.monpoke, command.hp, command.ap)
      existing_team = game_state.teams.find {|team| team.name == command.team}
      if existing_team
        existing_team.monpokes << monpoke
      else
        raise StandardError.new("No more than two teams") if game_state.teams.length >= 2
        game_state.teams << Team.new(command.team, monpoke)
      end
      puts "#{command.monpoke} has been assigned to team #{command.team}!"

    when :ICHOOSEYOU
      team = game_state.teams[game_state.turn]
      selected_mon = team.monpokes.find{ |m| m.name == command.monpoke }
      raise StandardError.new("You may only choose your own Monpoke") unless selected_mon
      team.selected_mon = selected_mon
      puts "#{selected_mon.name} has entered the battle!"
      next_turn

    when :ATTACK
      attacking_team = game_state.teams[game_state.turn]
      attacked_team = game_state.teams[game_state.next_turn]
      raise StandardError.new("A defeated Monpoke cannot attack!") if attacking_team.selected_mon.hp <= 0
      attacked_team.selected_mon.take_damage(attacking_team.selected_mon.ap)
      puts "#{attacking_team.selected_mon.name} attacked #{attacked_team.selected_mon.name} for #{attacking_team.selected_mon.ap}!" #(#{attacked_team.selected_mon.hp} health remaining."
      puts "#{attacked_team.selected_mon.name} has been defeated!" if attacked_team.selected_mon.hp <= 0
      next_turn

    else
      raise StandardError.new("Unrecogized command type: #{command.type}")
    end
  end

  private

  def next_turn
    game_state.turn = game_state.next_turn
  end
end
