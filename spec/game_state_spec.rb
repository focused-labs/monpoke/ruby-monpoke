describe "GameState" do
  game_state = nil

  before do
    game_state = GameState.new
  end

  it "has zero to two teams" do
    expect(game_state.teams.empty?).to be_truthy
  end

  it "can take turns" do
    expect(game_state.turn).to eq(0)
  end

  it "taking turns with one team does nothing" do
    game_state.teams << Team.new("abc", "def")
    expect(game_state.next_turn).to eq(0)
    game_state.turn = 1
    expect(game_state.next_turn).to eq(0)
  end

  it "can take turns with two teams" do
    game_state.teams << Team.new("abc", "def")
    game_state.teams << Team.new("ghi", "jkl")
    expect(game_state.next_turn).to eq(1)
    game_state.turn = game_state.next_turn
    expect(game_state.next_turn).to eq(0)
    game_state.turn = game_state.next_turn
    expect(game_state.next_turn).to eq(1)
  end
end

