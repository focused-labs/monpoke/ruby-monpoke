describe "GameRunner" do
  it "can read from a file" do
    controller = double("GameController")
    allow(File).to receive(:readlines).with('filename').and_return(['create Valiant Starmee 100 10'])
    runner = GameRunner.new('filename', controller)
    expect(controller).to receive(:exec_cmd).once
    runner.run
  end

  it "can read commands from the user" do
    controller = double("GameController")
    allow(STDIN).to receive(:gets).and_return('create Valiant Starmee 100 10', nil)
    runner = GameRunner.new(nil, controller)
    expect(controller).to receive(:exec_cmd).once
    expect{
      runner.run
    }.to output("Welcome to Monpoke!\nType commands below, CTRL-C to quit\n").to_stdout
    runner.run
  end
end

