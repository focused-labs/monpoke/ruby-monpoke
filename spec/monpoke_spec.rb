describe "Monpoke" do
  it "has a name" do
    blargazard = Monpoke.new "Blargazard"
    expect(blargazard.name).to eq("Blargazard")
  end
  it "has hp" do
    blargazard = Monpoke.new "Blargazard"
    expect(blargazard.hp).to eq(1)
  end
  it "has ap" do
    blargazard = Monpoke.new "Blargazard"
    expect(blargazard.ap).to eq(1)
  end
end

