require_relative 'command'
require_relative 'game_controller'
require_relative 'game_state'
require_relative 'monpoke'
require_relative 'team'

class GameRunner
  attr_accessor :input_file, :game_controller

  def initialize(input_file=nil, game_controller=GameController.new)
    self.input_file = input_file
    self.game_controller = game_controller
  end

  def run
    if input_file
      File.readlines(input_file).each do |cmd|
        run_command cmd
      end
    else
      puts "Welcome to Monpoke!"
      puts "Type commands below, CTRL-C to quit"
      while true
        command = STDIN.gets
        break if command.nil?
        run_command command.chomp
      end
    end
  end

  def run_command(cmd)
    args = cmd.split.map
    cmd = Command.new *args
    game_controller.exec_cmd cmd
  end
end

