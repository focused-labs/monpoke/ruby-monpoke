# Welcome to Monpoke!

This is a simple ruby program that plays the Monpoke game with one or two players. 

## How to run the app

In the root directory of the project you will find a shell script `monpoke`

### Single-player mode

1. Write monpoke commands into a text file, following the instructions in `monpoke.pdf`
2. Then run `monpoke` with a relative path to the file, e.g.

````
$ ./monpoke data/commands.txt
````

This will run all the the commands in the file, ending when either the game ends or the end of the file is reached.

### Two-player mode

Since there is no common database, both players must have access to the same directory on the mac, either:
* sitting next to each other passing the keyboard back and forth 
* using a paring app such as `Tuple` to share the keyboard

Run `monpoke` with no arguments:

````
$ ./monpoke
````

and then type in commands one by one.

## How to run the specs

In the project root run

````
$ rspec spec
````
