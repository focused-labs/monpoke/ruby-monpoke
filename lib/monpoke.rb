class Monpoke
  attr_accessor :name, :hp, :ap

  def initialize(name, hp=1, ap=1)
    self.name = name
    self.hp = hp.to_i
    self.ap = ap.to_i
  end

  def take_damage(incoming_ap)
    self.hp -= incoming_ap
  end

end
