describe "Team" do
  green = nil
  before do
    green = Team.new "Green", Monpoke.new("Bulbachu")
  end
  it "has a roster" do
    expect(green.monpokes.length).to equal(1)
  end
  it "has a name" do
    expect(green.name).to eq("Green")
  end
end

