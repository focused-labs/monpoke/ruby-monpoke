class GameState
  attr_accessor :teams, :turn

  def initialize
    self.teams = []
    self.turn = 0
  end

  def next_turn
    (turn + 1) % teams.length
  end
end
