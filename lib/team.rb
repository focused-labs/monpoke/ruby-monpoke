class Team
  attr_accessor :name
  attr_accessor :monpokes
  attr_accessor :selected_mon

  def initialize(name, monpoke)
    self.name = name
    self.monpokes = [monpoke]
  end
end
