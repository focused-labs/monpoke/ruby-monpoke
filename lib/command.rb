class Command
  attr_accessor :type, :team, :monpoke, :hp, :ap

  def initialize(type, *args)
    self.type = type.upcase.to_sym
    case self.type
    when :CREATE
      self.team = args[0]
      self.monpoke = args[1]
      self.hp = args[2]
      self.ap = args[3]
    when :ICHOOSEYOU
      self.monpoke = args[0]
    when :ATTACK
    else
      raise StandardError.new("Unrecognized command type: #{self.type}")
    end
  end
end

