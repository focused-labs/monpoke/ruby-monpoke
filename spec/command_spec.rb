require './lib/command'

describe "Command" do
  describe "CREATE" do
    create_cmd = nil
    before do
      create_cmd = Command.new(:CREATE, "Green", "Bulbachu")
    end
    it "has a type CREATE" do
      expect(create_cmd.type).to eq :CREATE
    end
    it "has a monpoke" do
      expect(create_cmd.monpoke).to eq("Bulbachu")
    end

    it "has a team" do
      expect(create_cmd.team).to eq("Green")
    end
  end
  describe "ICHOOSEYOU" do
    choose_cmd = Command.new(:ICHOOSEYOU, "Bulbachu")
    it "has a type" do
      expect(choose_cmd.type).to eq(:ICHOOSEYOU)
    end
    it "has a monpoke" do
      expect(choose_cmd.monpoke).to eq("Bulbachu")
    end
  end
  describe "ATTACK" do
    choose_cmd = Command.new(:ATTACK)
    it "has a type" do
      expect(choose_cmd.type).to eq(:ATTACK)
    end
  end
end
