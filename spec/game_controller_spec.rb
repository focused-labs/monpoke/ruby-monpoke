describe "GameController" do
  let (:game) { GameController.new }

  it "has a GameState" do
    expect(game.game_state.class).to eq(GameState)
  end
  describe "CREATE execution" do
    create_green = Command.new(:CREATE, "Green", "Bulbachu")
    create_blue = Command.new(:CREATE, "Blue", "Squidgy")
    create_chartreuse = Command.new(:CREATE, "Chartreuse", "Friggle")

    it "adds a team to its game state" do
      game.exec_cmd create_green
      expect(game.game_state.teams.length).to equal(1)
      expect(game.game_state.teams.first.name).to eq("Green")
    end

    it "adds additional monpokes to the correct team" do
      create_green_blargazard = Command.new(:CREATE, "Green", "Blargazard")
      game.exec_cmd create_green
      game.exec_cmd create_green_blargazard
      expect(game.game_state.teams.first.monpokes.length).to eq(2)
      second_monpoke = game.game_state.teams.first.monpokes.last
      expect(second_monpoke.name).to eq("Blargazard")
    end

    it "adds a new team when the command's team does not already exist" do
      game.exec_cmd create_green
      game.exec_cmd create_blue
      expect(game.game_state.teams.length).to eq(2)
      chartreuse = game.game_state.teams.find{|t| t.name == "Blue" }
      expect(chartreuse.monpokes.length).to eq(1)
      expect(chartreuse.monpokes.first.name).to eq("Squidgy")
    end

    it "allows ap and hp to be added at create time" do
      create_green_fraggle = Command.new(:CREATE, "Green", "Fraggle", 100, 10 )
      game.exec_cmd create_green_fraggle
      monpoke = game.game_state.teams[0].monpokes[0]
      expect(monpoke.hp).to eq(100)
      expect(monpoke.ap).to eq(10)
    end

    it "does not allow HP less than 1" do
      create_green_bazoomba = Command.new(:CREATE, "Green", "Bazoomba", 0, 1)
      expect { game.exec_cmd create_green_bazoomba }.to raise_error(StandardError, "HP and AP must be above 0")
    end

    it "does not allow AP less than 1" do
      create_green_bazoomba = Command.new(:CREATE, "Green", "Bazoomba", 1, 0)
      expect { game.exec_cmd create_green_bazoomba }.to raise_error(StandardError, "HP and AP must be above 0")
    end

    it "does not allow a third team to be added" do
      game.exec_cmd create_green
      game.exec_cmd create_blue
      expect { game.exec_cmd create_chartreuse }.to raise_error(StandardError,"No more than two teams")
    end

  end

  describe "ICHOOSEYOU execution" do
    create_green = Command.new(:CREATE, "Green", "Bulbachunk")
    create_blue = Command.new(:CREATE, "Blue", "Squidgy")

    before do
      game.exec_cmd create_green
      game.exec_cmd create_blue
    end

    it "assigns the chosen monpoke to the current team" do
      game.exec_cmd (Command.new(:ICHOOSEYOU, "Bulbachunk"))
      expect(game.game_state.teams.first.selected_mon.name).to eq("Bulbachunk")
    end

    it "does not let a team assign another team's monpoke" do
      cmd = Command.new(:ICHOOSEYOU, "Squidgy")
      expect{ game.exec_cmd cmd  }.to raise_error(StandardError, "You may only choose your own Monpoke")
    end

    context "players take turns" do
      it "knows whose turn it is" do
        game.exec_cmd (Command.new(:ICHOOSEYOU, "Bulbachunk"))
        expect(game.game_state.teams.first.selected_mon.name).to eq("Bulbachunk")
        expect(game.game_state.teams.last.selected_mon).to be_nil

        game.exec_cmd (Command.new(:ICHOOSEYOU, "Squidgy"))
        expect(game.game_state.teams.first.selected_mon.name).to eq("Bulbachunk")
        expect(game.game_state.teams.last.selected_mon.name).to eq("Squidgy")
      end
    end
  end

  describe "ATTACK execution" do
    game_state = GameState.new
    team1 = Team.new("Green", Monpoke.new("Bulbachunk", 100, 10))
    team2 = Team.new("Blue", Monpoke.new("Fraggle", 75, 20))
    game_state.teams << team1
    game_state.teams << team2

    game = GameController.new
    game.game_state = game_state
    game.exec_cmd(Command.new(:ICHOOSEYOU, "Bulbachunk"))
    game.exec_cmd(Command.new(:ICHOOSEYOU, "Fraggle"))

    it "the current team can attack" do
      current_hp = game_state.teams.last.selected_mon.hp
      current_ap = game_state.teams.first.selected_mon.ap
      game_state.turn = 0
      expect {
        game.exec_cmd(Command.new(:ATTACK))
      }.to output(/#{team1.monpokes[0].name} attacked #{team2.monpokes[0].name} for #{team1.monpokes[0].ap}!/ ).to_stdout
      expect(game_state.teams.last.selected_mon.hp).to eq(current_hp - current_ap)
    end

    it "can defeat an enemy monpoke" do
      team2.monpokes[0].hp = 5
      game_state.turn = 0
      expect {
        game.exec_cmd(Command.new(:ATTACK))
      }.to output(/#{team2.monpokes[0].name} has been defeated!/ ).to_stdout
      expect(game_state.teams.last.selected_mon.hp).to eq(-5)
    end

    it "a defeated monpoke cannot attack" do
      team1.monpokes[0].hp = -10
      game_state.turn = 0
      expect {
        game.exec_cmd(Command.new(:ATTACK))
      }.to raise_error(StandardError, "A defeated Monpoke cannot attack!")
    end
  end

end

